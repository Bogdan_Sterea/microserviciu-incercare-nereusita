# Version: 0.0.1
FROM java:8
MAINTAINER  George Boboc <george.boboc@metrosystems.net>


ADD logback.xml /data/logback.xml
ADD target/SimpleRest.jar /data/SimpleRest.jar
RUN bash -c 'touch /data/SimpleRest.jar'
RUN mkdir -p /logdata
WORKDIR /data
VOLUME /logdata
CMD java -jar -Djava.security.egd=file:/dev/./urandom -Dlogging.config=/data/logback.xml /data/SimpleRest.jar
EXPOSE 8080