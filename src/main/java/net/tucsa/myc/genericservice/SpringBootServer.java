package net.tucsa.myc.genericservice;

import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;

import net.tucsa.myc.genericservice.repository.Database;

@Configuration
@EnableAutoConfiguration
@EnableCircuitBreaker
@EnableHystrixDashboard
@EnableScheduling
//@SpringBootApplication
public class SpringBootServer {
	
	@Autowired
	private Environment env;
	
//	@Autowired
//	private Database database;
//	
	
	final static Logger logger = LoggerFactory.getLogger(SpringBootServer.class);
	
	
	
    public static void main(String[] args) {
        SpringApplication.run(SpringBootServer.class, args);
    }

    
//    @Bean
//	public DataSource dataSource() {
//		
//		String driver = env.getProperty("database.driver");
//		String name = env.getProperty("database.name").toLowerCase();
//		String schema = env.getProperty("database.schema");
//		String path = env.getProperty("database.url");
//		String username = env.getProperty("database.user");
//		String password = env.getProperty("database.password");
//		
//		// Se incearca conectarea la BD. 
//		// Daca nu se reuseste, se creaza o noua BD.
//		database.tryToConnectIfCouldntCreateANewOne(path, driver, name, schema, username, password);
//		
//	    return DataSourceBuilder
//		        .create()
//		        .username(username)
//		        .password(password)
//		        .url(path + name)
//		        .driverClassName(driver)
//		        .build();
//	}
	
//	 @Bean
//	 public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
//	     
//		String schema = env.getProperty("database.schema");
//		 
//		LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
//		lef.setDataSource(dataSource);
//		lef.setJpaVendorAdapter(jpaVendorAdapter);
//		lef.setPackagesToScan("com.myc");
//		Properties jpaProperties = new Properties();
//		jpaProperties.setProperty("hibernate.hbm2ddl.auto", "update");
//		jpaProperties.setProperty("hibernate.default_schema", schema);
//		lef.setJpaProperties(jpaProperties);
//		return lef;
//	    }
    
    
}