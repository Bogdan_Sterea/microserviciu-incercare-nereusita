package net.tucsa.myc.genericservice.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class Database {
	
	private Connection conn;
	
	final static Logger logger = LoggerFactory.getLogger(Database.class);
	
	
	public boolean tryToConnectIfCouldntCreateANewOne(String path, String driver, String name, String schema, String username, String password) {
			
		// Se incearca conectarea la baza de date indicata in application.properties
		try {
			
			Class.forName(driver);
			conn = DriverManager.getConnection(path + name, username, password);
			
		}
		catch (Exception e1)  {
				
			// In caz de eroare, se incearca conectarea la baza de date default "postgres"
			// Contul la serverul PostgreSQL (username + password) trebuie sa aiba drepturi de a crea BD
			// Apoi se creaza o noua baza de date si schema corespunzatoare
			try {
				
				logger.info("Couldn't connect to DB with parameters in application.properties! Exception " + e1);
				conn = DriverManager.getConnection(path + "postgres", username, password);

				try {
					Statement statement = conn.createStatement();
					statement.execute("CREATE DATABASE " + name);
					conn.close();
                    
                    conn = DriverManager.getConnection(path+name, username, password);
                    statement = conn.createStatement();
                    statement.execute("CREATE SCHEMA IF NOT EXISTS " + schema);
					conn.close();
					
					logger.info("Database \"" + name + "\" and schema \"" + schema + "\" were created.");
					
				}
				catch(Exception e2)
				{
					logger.error("Could not create new database! Exception: " + e2);
					return false;
				}
			}
			catch (Exception e3)
			{
				
				logger.error("Could not connect to default database! Exception: " + e3);
				return false;
			}
		}
				
		try {
			conn.close();
		} catch (SQLException e4) {
			logger.error("Failed to close connection! Exception: " + e4);
		}
		return true;
	}
}