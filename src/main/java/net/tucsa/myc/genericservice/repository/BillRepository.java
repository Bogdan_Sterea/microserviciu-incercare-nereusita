package net.tucsa.myc.genericservice.repository;

import java.util.Date;
import java.util.List;


import net.tucsa.myc.genericservice.model.Bill;

//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;



public interface BillRepository extends CrudRepository<Bill, Long>{
	
	List<Bill> findByNameOrderByNameAsc(String name);

	List<Bill> findByCuiOrderByDateAsc(String cui);
	
	List<Bill> findAllByOrderByIdAsc();

//	Exemplu
//	@Query(value = "SELECT * FROM BILL WHERE date >= ?1 and date < ?2", nativeQuery = true)
//	List<Bill> findByInterval(Date startDate, Date endDate);
	
	List<Bill> findByDateBetweenOrderByDateAsc(Date startDate, Date endDate);

}
