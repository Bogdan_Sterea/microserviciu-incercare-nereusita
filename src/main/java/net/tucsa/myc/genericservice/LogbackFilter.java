package net.tucsa.myc.genericservice;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;

import com.eaio.uuid.UUID;

@WebFilter("/*")
public class LogbackFilter implements Filter {

    public static final String SERVICE_VERSION = "@service-version";
    public static final String REQUEST_ID = "@trace-id";
    public static final String REQUEST_ID_HEADER = "dr-trace-id";

    private String serviceVersion;

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException,
            ServletException {

        String uuid = readOrCreateUuid((HttpServletRequest)servletRequest);

        MDC.put(REQUEST_ID, uuid.toString());
        MDC.put(SERVICE_VERSION, serviceVersion);

        servletRequest.setAttribute(REQUEST_ID, uuid);

        try {
          filterChain.doFilter(servletRequest, servletResponse);
        } finally {
          MDC.remove(REQUEST_ID);
          MDC.remove(SERVICE_VERSION);
        }
    }

    private String readOrCreateUuid(HttpServletRequest request) {
        String uuid = request.getHeader(REQUEST_ID_HEADER);
        if (uuid != null) {
            return uuid;
        }
        Object uuidObj = request.getAttribute(REQUEST_ID);
        if (uuidObj == null || !(uuidObj instanceof String)) {
            return new UUID().toString();
        }
        else {
            return uuidObj.toString();
        }
    }

    public void destroy() {
    }

    public void setServiceVersion(String serviceVersion) {
        this.serviceVersion = serviceVersion;
    }
}