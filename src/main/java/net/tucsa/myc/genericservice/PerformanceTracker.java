package net.tucsa.myc.genericservice;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class PerformanceTracker {

    final static Logger logger = LoggerFactory.getLogger(PerformanceTracker.class);

    public static final String REQUEST_ID = "trace-id";
    public static final String LOG_PREFIX = "(tracking)==>";

    public static void track(String description, long start, long end) {
        String duration = "[" + (end - start) + " ms.]";
        logger.info(LOG_PREFIX + " " + getTrackingID() + " " + duration + " " + description);
    }

    public static void start() {
        String mdcID = MDC.get(REQUEST_ID);
        if(StringUtils.isEmpty(mdcID)){
            MDC.put(REQUEST_ID, UUID.randomUUID().toString());
            mdcID = MDC.get(REQUEST_ID);
        }
        logger.info(LOG_PREFIX + " " + getTrackingID() + " ==> start tracking ...");
    }

    protected static String getTrackingID() {
        return "ID='" + MDC.get(REQUEST_ID) + "'";
    }    
}
