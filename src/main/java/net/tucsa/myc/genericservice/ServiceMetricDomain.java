package net.tucsa.myc.genericservice;

import net.tucsa.myc.starter.metric.MetricFactory.MetricDomain;

public enum ServiceMetricDomain implements MetricDomain { CREDIT_ACCOUNT }