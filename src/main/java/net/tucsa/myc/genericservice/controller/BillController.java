package net.tucsa.myc.genericservice.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import net.tucsa.myc.genericservice.repository.Database;
import net.tucsa.myc.genericservice.model.Bill;
import net.tucsa.myc.genericservice.repository.BillRepository;

@RestController
public class BillController {
	
	@Autowired
	BillRepository billRepository;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	Database database;
	
	final static Logger logger = LoggerFactory.getLogger(BillController.class);
	
	@RequestMapping(value = "/bills/init", method = RequestMethod.GET)
	public String process(){
		billRepository.save(new Bill("Conbustibil", "CUI_0000", 350, new Date()));
		billRepository.save(new Bill("Conbustibil", "CUI_1111", 350, new Date()));
		billRepository.save(new Bill("Recuzita", "CUI_2222", 12, new Date()));
//		billRepository.save(new Bill("Telefon", "CUI_3333", 570, new Date()));
//		billRepository.save(new Bill("Cauciucuri", "CUI_4444", 490, new Date()));
//		billRepository.save(new Bill("Scule", "CUI_5555", 321, new Date()));
		
		return "Done";
	}
	
	
	@ApiOperation(
            value = "Read all bills",
            produces = "application/json" )
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    @RequestMapping(
            value= { "/bills" },
            method = { RequestMethod.GET },
            produces = { "application/json" })
	public ResponseEntity<List<Bill>> findAll(){
		List<Bill> listBills = new ArrayList<Bill>();
		billRepository.findAllByOrderByIdAsc().
		forEach(listBills::add);
		return new ResponseEntity<List<Bill>>(listBills, HttpStatus.OK);
	}
	
	
	@ApiOperation(
            value = "Read one bill",
            produces = "application/json" )
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    @RequestMapping(
            value= { "/bills/{id}" },
            method = { RequestMethod.GET },
            produces = { "application/json" })
	public ResponseEntity<Bill> findById(@PathVariable("id") long id){
		return new ResponseEntity<Bill>(billRepository.findOne(id), HttpStatus.OK);
	}
	
	
	@ApiOperation(
            value = "Save a bill", 
            notes = "id and date are not required",
            consumes = "application/json",
            produces = "application/json" )
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Created") })
    @RequestMapping(
            value= { "/bills" },
            method = { RequestMethod.POST },
            consumes = { "application/json" },
            produces = { "application/json" })
	public ResponseEntity<Bill> createBill(@RequestBody(required = true) Bill bill, UriComponentsBuilder ucBuilder) {
       
		/*List<Bill> listBills = billRepository.findByCui(bill.getCui());
        if (listBills.size() > 0){
        	logger.info("Unable to save. A Bill with CUI " + bill.getCui() + " already exist");
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }*/
        Date date = new Date();
        bill.setDate(date);
        billRepository.save(bill);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/bills/{id}").buildAndExpand(bill.getId()).toUri());
        return new ResponseEntity<Bill>(bill, headers, HttpStatus.CREATED);
	}
	
	
	/**
	  * PUT /update  --> Update a bill by modifying it's fields, after reading it from database. 
	  * (default way of updating for Spring Data JPA).
	  */
	@ApiOperation(
            value = "Update a bill",
            consumes = "application/json",
            produces = "application/json" )
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
    						@ApiResponse(code = 400, message = "Unable to update. Different id between PathVariable (id) and entity id field"),
    						@ApiResponse(code = 404, message = "Resource with specified id not found")})
    @RequestMapping(
            value= { "/bills/{id}" },
            method = { RequestMethod.PUT },
            consumes = { "application/json" },
            produces = { "application/json" })
	public ResponseEntity<Bill> update(@RequestBody(required = true) Bill bill, @PathVariable("id") long id) {
		
		Bill billToUpdate = billRepository.findOne(bill.getId());
		
		if (billToUpdate == null) {
			logger.info("Unable to update. Bill with id " + id + " not found");
            return new ResponseEntity<Bill>(HttpStatus.NOT_FOUND);
        }
		
		if (id != bill.getId()) {
			logger.info("Unable to update. Different value between PathVariable (id) and entity id field");
            return new ResponseEntity<Bill>(HttpStatus.BAD_REQUEST);
		}
		
		billToUpdate.setCui(bill.getCui());
		billToUpdate.setName(bill.getName());
		billToUpdate.setValue(bill.getValue());
		
		billRepository.save(billToUpdate);
		
		return new ResponseEntity<Bill>(billToUpdate, HttpStatus.OK);
	}
	
	
	@ApiOperation(
            value = "Delete a bill",
            produces = "application/json" )
    @ApiResponses(value = { @ApiResponse(code = 204, message = "OK. Bill deleted."),
    						@ApiResponse(code = 404, message = "Resource with specified id not found")})
    @RequestMapping(
            value= { "/bills/{id}" },
            method = { RequestMethod.DELETE },
            produces = { "application/json" })
	public ResponseEntity<?> delete(@PathVariable("id") long id){
		
		Bill billToDelete = billRepository.findOne(id);
        
		if (billToDelete == null) {
			logger.info("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<Bill>(HttpStatus.NOT_FOUND);
        }
  
		billRepository.delete(id);
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}
	
	/**
	  * GET /find bills by cui  --> Search in database for bills with specified cui
	  */
	@ApiOperation(
            value = "Find bills by cui",
            produces = "application/json" )
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	@RequestMapping(value = "/bills/findbycui", method = RequestMethod.GET)
	public ResponseEntity<List<Bill>> findByCui(@RequestParam("cui") String cui){
		List<Bill> listBills = new ArrayList<Bill>();
		billRepository.findByCuiOrderByDateAsc(cui).
		forEach(listBills::add);
		return new ResponseEntity<List<Bill>>(listBills, HttpStatus.OK);
	}
	
    
	@ApiOperation(
            value = "Find bills between dates",
            produces = "application/json" )
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    @RequestMapping(value = "/bills/findbydate", method = RequestMethod.GET)
	public ResponseEntity<List<Bill>> findByDate(
			@RequestParam("sdate") @DateTimeFormat(pattern = "dd/MM/yyyy") Date sdate,
			@RequestParam("edate") @DateTimeFormat(pattern = "dd/MM/yyyy") Date edate){
		List<Bill> listBills = new ArrayList<Bill>();
			 
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(edate);
//		cal.add(Calendar.DATE, 1); // add 1 day
//		edate = cal.getTime();
		
		billRepository.findByDateBetweenOrderByDateAsc(sdate, edate).
		forEach(listBills::add);
		return new ResponseEntity<List<Bill>>(listBills, HttpStatus.OK);
	}
    
    
    
}

